<?php include('header.php'); ?>

	<div class="main">

		<div class="container">

				<div class="assessmeent-status">
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" id="assessment-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width:2em;">
					    0%
					  </div>
					</div>
				</div>



				<div id="assessment-panel">
						
					
				</div>
				<div class="assessment-nav">
					<button id="next-question">NEXT QUESTION</button>
				</div>
		</div>
	</div>

<?php include('footer.php'); ?>



$( document ).ready(function() {




	/* loading demographics page on load as the first page */
    $( "#assessment-panel" ).load( "pages/demographics.php" );

    /* making the whole set div selectable */

   $(document).on('click', '.set-item', function() { 

   	$(this).addClass('selected');
   	$(this).siblings('.set-item').removeClass('selected');
   	$(this).find(':radio').prop("checked", true);

   	

   	/* automatically moves the sets to the next one after inputing answer */

      if($(this).closest('.question-set').hasClass('radio-option')){

     	  $(this).closest('.question-set').delay( 800 ).effect('slide', { direction: 'left', mode: 'hide' },function(){
     		 $(this).closest('.question-set').next().effect('slide', { direction: 'left', mode: 'show' });
      	});

      }

    });


   /* dynamically change the avaibale option on ranking select fields 


   $(document).on('change', '.rank-select', function() {

    if($(this).val()!='0'){
      $(this).closest('.set-item').siblings('.set-item').find('.rank-select option[value="'+$(this).val()+'"]').hide();
    }

   });

*/


/* sortable sets */
 $(document).on('mousedown', '#sortable-set', function() { 
  
    $( "#sortable-set" ).sortable();
    $( "#sortable-set" ).disableSelection();

     });






    

    /* treiggering form submit on the current page */
	$( "#next-question" ).click(function() {
	  $( "#assessment-panel form" ).submit();
	});


	/* on submit form ajax process */

    $(document).on('submit', '#assessment-panel form', function(e) { 

    	//alert( $(this).serialize());

    	$.ajax({
            url     : 'includes/ajax-request-receiver.php',
            type    : 'POST',
            /*dataType: 'json',*/
            data    : $(this).serializeArray(),
            beforeSend: function() {        
                    
                    alert('submitting form');

                     
                    
               },
            success : function( data ) {
                         alert('succes - '+data);
                         $( "#assessment-panel" ).load( "pages/"+data+".php", function(){
                         	$('.question-set').not(':eq(0)').hide();
                         } );

                         //$currentProgress = parseInt( $('#assessment-progress').attr('aria-valuenow'));
                         $newProgress = parseInt( $('#assessment-progress').attr('aria-valuenow')) + 17;
                         $('#assessment-progress').attr('aria-valuenow',$newProgress);
                         $('#assessment-progress').css('width',$newProgress+'%');
                         $('#assessment-progress').html($newProgress+'%');

            },
            error   : function( xhr, err ) {
                         alert(xhr+' - '+err);     
            }
        });    

    	 e.preventDefault();
    });


    /*
		
		$(document).on('submit', '#assessment-panel form', function() {            
        $.ajax({
            url     : $(this).attr('action'),
            type    : $(this).attr('method'),
            dataType: 'json',
            data    : $(this).serialize(),
            success : function( data ) {
                         alert('Submitted');
            },
            error   : function( xhr, err ) {
                         alert('Error');     
            }
        });    
        return false;
    });

    */	

});
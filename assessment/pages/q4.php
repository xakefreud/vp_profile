<div class="assessment-part assessment-q4">
	<h2 class="assessment-part-title">Question 4:</h2>
	<p class="assessment-part-instruction">Rank the following statements in order from 1 to 7 with 1 being most preferred and 7 being least preferred</p>


	<form class="">

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 1:</span>
			</div>
			<div class="col-xs-12 set-item">
				<p>To be careful and act with caution to feel secure</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be kind and act friendly to feel I belong</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be responsible and act decisively to feel courage</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be structured and act reliably to feel certain</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be curious and act with flexibility to feel knowledgeable</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be understanding and act with compassion to feel grateful</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be calm and act with openness to feel detachment</p>
				
			</div>

			
			

		</div>

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 2:</span>
			</div>
			<div class="col-xs-12 set-item">
				<p>To be modest and act with moderation to feel hope</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be caring and act with forgiveness to feel love</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be confident and act assertively to feel worthy</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be honest and act fairly to feel trustworthy</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be driven and act with initiative to feel successful</p>
				
			</div>

			

			<div class="col-xs-12 set-item">
				<p>To be empathetic and act with service to feel harmony</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be tolerant and act with discovery to feel humility</p>
				
			</div>

		</div>

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 3:</span>
			</div>
			<div class="col-xs-12 set-item">
				<p>To be mindful and act with self-discipline to feel vital</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be thoughful and act helpful to feel supportive</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To be determined and act with perseverance to feel resilient</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be balanced and act with integrity to feel honourable</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be creative and act resourcefully to feel wise</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be accepting and act with purpose to feel peaceful</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To be patient and act with adaptibility to feel contentment</p>
				
			</div>

		</div>

		<input type="hidden" name="current-page" value="q4">

	</form>

</div>
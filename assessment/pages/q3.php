<div class="assessment-part assessment-q3">

	
		<h2 class="assessment-part-title">Question 3:</h2>
		<p class="assessment-part-instruction">Out of the following three options select one that you most prefer?</p>

	<form class="">

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 1:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-1" > To be careful and act with caution to feel secure</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-1" > To be modest and act with moderation to feel hope</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-1" > To be mindful and act with self-discipline to feel vital</p>
				
			</div>
		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 2:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2"> To be kind and act friendly to feel I belong</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2"> To be caring and act with forgiveness to feel love</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2"> To be thoughful and act helpful to feel supportive</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 3:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3"> To be responsible and act decisively to feel courage</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3" > To be confident and act assertively to feel worthy</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3"> To be determined and act with perseverance to feel resilient</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 4:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4"> To be structured and act reliably to feel certain</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4 set-item"> To be honest and act fairly to feel trustworthy</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4"> To be balanced and act with integrity to feel honourable</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number set-item">
				<span>Set 5:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-5"> To be curious and act with flexibility to feel knowledgeable</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-5"> To be driven and act with initiative to feel successful</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-5"> To be creative and act resourcefully to feel wise</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 6:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6">To be calm and act with openness to feel detachment</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6">To be tolerant and act with discovery to feel humility</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6"> To be patient and act with adaptibility to feel contentment</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 7:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7"> To be calm, tolerant and patient</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7"> To act with openness, discovery and adaptibility</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7"> To have detachment, humility and contentment</p>
				
			</div>
		</div>


		<input type="hidden" name="current-page" value="q3">

	</form>

</div>

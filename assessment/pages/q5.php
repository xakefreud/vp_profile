<div class="assessment-part assessment-q4">
	<h2 class="assessment-part-title">Question 5:</h2>
	<p class="assessment-part-instruction">For each set of words select ONE that most represents you</p>


	<form class="">

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 1:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Careful</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Modest</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Mindful</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Cautious</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Moderation</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Self-discipline</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Security</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Hopeful</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Vitality</p>
				
			</div>

			
			

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 2:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Kindness</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Caring</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Thoughtful</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Friendliness</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Forgiveness</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Helpful</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Belonging</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Love</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Support</p>
				
			</div>

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 3:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Responsibility</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Confidence</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Determination</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Decisiveness</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Assertive</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Perseverance</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Courage</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Worthiness</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Resilience</p>
				
			</div>

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 4:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Structure</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Honesty</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Balance</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Reliability</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Fairness</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Integrity</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Certainty</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Trust</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Honour</p>
				
			</div>

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 5:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Curiosity</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Driven</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Creative</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Flexibility</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Initiative</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Resourcefulness</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Knowledgeable</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Success</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Wisdom</p>
				
			</div>

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 6:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Understanding</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Empathy</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Acceptance</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Compassion</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Service</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Purposefulness</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Gratitude</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Peace</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Vitality</p>
				
			</div>

		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 7:</span>
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Calmness</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Tolerance</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Patience</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Openness</p>
				
			</div>
			<div class="col-xs-6 col-sm-4 set-item">
				<p>Discovery</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Adaptability</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Detachment</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Humility</p>
				
			</div>

			<div class="col-xs-6 col-sm-4 set-item">
				<p>Contentment</p>
				
			</div>

		</div>

		<input type="hidden" name="current-page" value="q5">

	</form>

</div>
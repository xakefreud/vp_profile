<div class="assessment-part assessment-q1">

	
		<h2 class="assessment-part-title">Question 1:</h2>
		<p class="assessment-part-instruction">Out of the following three options select one that you most prefer?</p>

	<form class="">

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 1:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item radio-">
				<p><input type="radio" name="set-1" value="be"> To be careful, modest and mindful</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-1" value="do"> To act with caution, moderation and self-discipline</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-1" value="have"> To have security, hope and vitality</p>
				
			</div>
		</div>

		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 2:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2" value="do"> To act with friendliness, forgiveness and helpfulness</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2" value="have"> To have belonging, love and support</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-2" value="be"> To be kind, caring and thoughtful</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 3:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3" value="have"> To have courage, worthiness and resilience</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3" value="be"> To be responsible, confident and determined</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-3" value="do"> To act with decisiveness, assertiveness and perseverance</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 4:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4" value="be"> To be structured, honest and balanced</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4 set-item" value="have"> To have certainty, trust and honour</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-4" value="do"> To act reliably, fairly and with integrity</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number set-item">
				<span>Set 5:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-5" value="have"> To have knowledge, success and wisdom</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-5" value="do">To act with flexibility, initiative and resourcefulness</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p>To be curious, driven and creative</p>
				<input type="radio" name="set-5" value="be"> To be curious, driven and creative
			</div> 
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 6:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6" value="do">To act with compassion, service and purposefulness</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6" value="be"> To be understanding, empathetic and accepting</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-6" value="have"> To have gratitude, harmony and peace</p>
				
			</div>
		</div>


		<div class="row question-set radio-option">
			<div class="col-xs-12 set-number">
				<span>Set 7:</span>
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7" value="be"> To be calm, tolerant and patient</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7" value="do"> To act with openness, discovery and adaptibility</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 set-item">
				<p><input type="radio" name="set-7" value="have"> To have detachment, humility and contentment</p>
				
			</div>
		</div>


		<input type="hidden" name="current-page" value="q1">

	</form>

</div>


<div class="assessment-part assessment-demographics">

	
		<h2 class="assessment-part-title">Profile Information</h2>
		<p class="assessment-part-instruction">Please complete the data below to begin your profile.  Once complete you will be presented with the questions to start answering.  It should take you around 20 minutes to complete and it must be completed in one sitting.</p>

	<form class="">

	<fieldset>
		<label>First Name</label>
		<input type="text" name="first-name">
	</fieldset>

	<fieldset>
		<label>Last Name</label>
		<input type="text" name="last-name">
	</fieldset>

	<fieldset>
		<label>Gender</label>
		<input type="radio" name="gender" value="male"> Male

		<input type="radio" name="gender" value="female"> Female

	</fieldset>


	<fieldset>
		<label>Age Range</label>
		<select name="age-bracket">
			<option value="<18">Less than 18</option>
			<option value="18-25">18 - 25</option>
			<option value="26-30">26 - 30</option>
			<option value="31-35">31 - 35</option>
		</select>
	</fieldset>

	<fieldset>
		<label>Country of Residence</label>
		<select name="country" id="country"></select>
	</fieldset>

	<fieldset>
		<label>State</label>
		<select name="state" id="state"></select>
	</fieldset>



	<fieldset>
		<label>Email</label>
		<input type="email" name="email">
	</fieldset>

	<fieldset>
		<label>Re-enter you Email</label>
		<input type="email">
	</fieldset>

	<input type="hidden" name="current-page" value="demographics">
		
	</form>
</div>

<script src="assets/js/countries.js"></script>





<div class="assessment-part assessment-q2">
	<h2 class="assessment-part-title">Question 2:</h2>
	<p class="assessment-part-instruction">Rank the following statements in order from 1 to 7 with 1 being most preferred and 7 being least preferred</p>


	<form class="">

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 1:</span>
			</div>

			<div class="col-xs-12 sortable-container">
				<ul id="sortable-set">
					<li class="set-item"><span>
						<p>To be careful, modest and mindful</p>
						<!--
						<select class="rank-select" name="lens1">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>
					<li class="set-item"><span>
						<p>To be kind, caring and thoughtful</p>
						<!--
						<select class="rank-select" name="lens2">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>
					<li class="set-item"><span>
						<p>To be responsible, confident and determined</p>
						<!--
						<select class="rank-select" name="lens3">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>

					<li class="set-item"><span>
						<p>To be structured, honest and balanced</p>
						<!--
						<select class="rank-select" name="lens4">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>

					<li class="set-item"><span>
						<p>To be curious, driven and creative</p>
						<!--
						<select class="rank-select" name="lens5">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>

					<li class="set-item"><span>
						<p>To be understanding, empathetic and accepting</p>
						<!--
						<select class="rank-select" name="lens6">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>
						-->
	</span>					
					</li>

					<li class="set-item"><span>

						<p>To be calm, tolerant and patient</p>
						<!--
						<select class="rank-select" name="lens7">
								<option value="0" selected="selected">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
												
						</select>

						-->
	</span>					
					</li>

				</ul>

			</div>

		</div>

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 2:</span>
			</div>
			<div class="col-xs-12 set-item">
				<p>To act with caution, moderation and self-discipline</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To act with friendliness, forgiveness and helpfulness</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To act with decisiveness, assertiveness and perseverance</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To act reliably, fairly and with integrity</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To act with flexibility, initiative and resourcefulness</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To act with compassion, service and purposefulness</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To act with openness, discovery and adaptibility</p>
				
			</div>

		</div>

		<div class="row question-set">
			<div class="col-xs-12 set-number">
				<span>Set 3:</span>
			</div>
			<div class="col-xs-12 set-item">
				<p>To have security, hope and vitality</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To have belonging, love and support</p>
				
			</div>
			<div class="col-xs-12 set-item">
				<p>To have courage, worthiness and resilience</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To have certainty, trust and honour</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To have knowledge, success and wisdom</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To have gratitude, harmony and peace</p>
				
			</div>

			<div class="col-xs-12 set-item">
				<p>To have detachment, humility and contentment</p>
				
			</div>

		</div>

		<input type="hidden" name="current-page" value="q2">

	</form>

</div>